module example.com/deepmocking

go 1.14

require (
	github.com/golang/mock v1.5.0
	github.com/smartystreets/goconvey v1.6.4
)
