package main

import (
	what "example.com/deepmocking/internal"
)

func main() {
	client := what.NewEBXClient(what.NewAdapter())
	client.DoForReal()
}
