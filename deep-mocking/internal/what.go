package what

import "fmt"

//go:generate mockgen -destination "mock/mock_what.go" . InterfaceType,InterfaceSubtype

// Concrete types
type pbxConPool struct {
	sub Rows
}

func (t *pbxConPool) GetSub() Rows {
	fmt.Println("Called real!")
	return t.sub
}

type Rows struct {
	a int
}

func (s *Rows) GetA() int {
	return s.a
}

// Adapter
func NewAdapter() *Adapter {
	return &Adapter{
		concrete: pbxConPool{},
	}
}

type Adapter struct {
	concrete pbxConPool
}

func (i *Adapter) GetSub() InterfaceSubtype {
	sub := i.concrete.GetSub()
	return &sub
}

// Interfaces
type InterfaceType interface {
	GetSub() InterfaceSubtype
}

type InterfaceSubtype interface {
	GetA() int
}

type EBXClient struct {
	t InterfaceType
}

func NewEBXClient(t InterfaceType) *EBXClient {
	return &EBXClient{t: t}
}

// Code
func (e *EBXClient) Whatever() {
	x := e.t.GetSub()
	fmt.Println(x.GetA())
}

func (e *EBXClient) DoForReal() {
	e.Whatever()
}
