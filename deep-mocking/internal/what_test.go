package what_test

import (
	"testing"

	what "example.com/deepmocking/internal"
	mock_internal "example.com/deepmocking/internal/mock"

	"github.com/golang/mock/gomock"
	. "github.com/smartystreets/goconvey/convey"
)

func TestWhatever(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	Convey("Whatever", t, func() {
		mock_subtype := mock_internal.NewMockInterfaceSubtype(ctrl)
		mock_subtype.EXPECT().GetA().AnyTimes()

		mock := mock_internal.NewMockInterfaceType(ctrl)
		mock.EXPECT().GetSub().AnyTimes().Return(mock_subtype)

		client := what.NewEBXClient(mock)
		client.Whatever()
	})
}
