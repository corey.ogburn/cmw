#!/bin/bash

if [ -z "$2" ]; then
    echo "Usage: $0 <bootstrap-server> <topic>"
    exit 1
fi
docker exec cmw-kafka kafka-topics --create --bootstrap-server "$1" --replication-factor 1 --partitions 100 --topic "$2"

# local: localhost:9092
# dev: d-gp2-cmwkfk2-1.imovetv.com
