#!/usr/bin/env python3

import random

# n = number of producer/consumers
# m = number of partitions
def makeAssignment(n, m):
    partitions = [-1] * m
    for i in range(0, n):
        assignment = random.randint(0, m-1)
        partitions[assignment] += 1
    return max(partitions)


if __name__ == "__main__":
    random.seed()
    for i in range(1, 2000):
        results = []
        for j in range(1, 200): # num trials
            results.append(makeAssignment(i, i))
        print(i, sum(results) / len(results))
