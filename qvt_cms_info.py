#!/usr/bin/env python3

import sys
import time
import re
import psycopg2
import psycopg2.extras
import timeago
import requests

# http://cbd46b77.cdn.cms.movetv.com/cms/publish3/container/scheduleqvt/61555145b8464f7d9dee92fe1053177b.qvt

def time_str(epoch):
    if epoch is None:
        return "None"
    ret = str(epoch)
    if epoch > 1000000000000:
        epoch /= 1000
    return ret + " - " + time.strftime('%Y-%m-%d %H:%M:%S %Z', time.localtime(epoch)) + " (" + timeago.format(epoch) + ")"

def status_code(uri):
    try:
        r = requests.head(uri)
        return r.status_code
    except requests.ConnectionError as e:
        return "connection error: " + str(e)

def main():
    if len(sys.argv) != 2:
        print("Usage: " + sys.argv[0] + " <qvt url>")
        sys.exit(1)
    uri = sys.argv[1]
    print("uri:", uri)

    print("current status code:", status_code(uri))

    schedule_id = re.findall(r'([^/]+)\.qvt$', uri)[0]
    print("schedule_id: ", schedule_id)

    conn = psycopg2.connect("host=p-sv1-dbp2-53.imovetv.com dbname=cmsmgt002 user=cms_troubleshoot_ro password=sh00trouble", options="-c search_path=pcbd46b77")
    cur = conn.cursor(cursor_factory = psycopg2.extras.DictCursor)
    sql = '''
    SELECT cms_container_schedule.*, cms_assets.*, cms_containers.*, cms_assets.timeshiftable as asset_timeshiftable, cms_containers.timeshiftable as container_timeshiftable
    FROM cms_container_schedule
        JOIN cms_assets on cms_assets.id = cms_container_schedule.asset_id
        JOIN cms_containers ON cms_containers.id=cms_container_schedule.container_id
        WHERE guid = %s
    '''
    cur.execute(sql, (schedule_id,))
    results = cur.fetchone()
    if results is None:
        print("No results from query")
        sys.exit(1)
    results = dict(results)

    print("asset_id:", results['asset_id'])
    print("asset type:", results['atype'])
    print("asset timeshiftable:", results['asset_timeshiftable'])
    print("asset vis_start:", time_str(results['vis_start']))
    print("asset vis_stop:", time_str(results['vis_stop']))
    print("visible:", results['visible'])
    print("schedule_start:", time_str(results['schedule_start']))
    print("schedule_stop:", time_str(results['schedule_stop']))
    print("expires:", time_str(results['expires']))
    print("channel timeshiftable:", results['container_timeshiftable'])
    print("channel lookback minutes:", results['lookback_minutes'])
    print("channel lookback hours:", results['lookback_minutes'] / 60)

    # print("results", results)

    cur.close()
    conn.close()

if __name__ == "__main__":
    main()
