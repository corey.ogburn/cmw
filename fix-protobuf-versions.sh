#!/bin/bash -ex

# Clean any old installed versions

rm -rf ~/go/src/google.golang.org/genproto
rm -rf ~/go/src/google.golang.org/grpc
rm -rf ~/go/src/google.golang.org/protobuf
rm -rf ~/go/src/github.com/grpc-ecosystem
rm -rf ~/go/src/github.com/golang/protobuf
rm -rf ~/go/pkg/darwin_amd64/google.golang.org/genproto
rm -f ~/go/bin/protoc-gen-go
rm -f ~/go/bin/protoc-gen-go-grpc

brew remove protobuf || :
brew remove protobuf@3.7 || :

# Install new versions

PROTOC_ZIP=protoc-3.13.0-osx-x86_64.zip
curl -OL https://github.com/protocolbuffers/protobuf/releases/download/v3.13.0/$PROTOC_ZIP
sudo unzip -o $PROTOC_ZIP -d /usr/local bin/protoc
sudo unzip -o $PROTOC_ZIP -d /usr/local 'include/*'
rm -f $PROTOC_ZIP

export GO111MODULE=on
go get github.com/golang/protobuf/protoc-gen-go@v1.3.5
