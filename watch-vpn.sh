#!/bin/bash

function success {
    echo -e -n "\033[0;32m\xE2\x9C\x94\033[0m"
}

function failure {
    echo -e -n "\xE2\x9D\x8C"
}

function try {
    if $1 >/dev/null 2>&1; then
        success
    else
        failure
    fi
    echo "  $1"
}


function commands {
    scutil --dns | egrep "resolver|nameserver|configuration"
    echo
    try "ping -c 1 d-gp2-cassdb1-1.imovetv.com"
    try "host d-gp2-cassdb1-1.imovetv.com"
    try "ping -c 1 q-gp2-cmwkfk3-1.imovetv.com"
    try "host q-gp2-cmwkfk3-1.imovetv.com"
    try "ping -c 1 d-gp2-salt-1.imovetv.com"
    try "host d-gp2-salt-1.imovetv.com"
    try "ping -c 1 p-gp2-bitbucket.imovetv.com"
    try "host p-gp2-bitbucket.imovetv.com"
    try "ping -c 1 q-gp2-cmwkfk3-5.imovetv.com"
    try "host q-gp2-cmwkfk3-5.imovetv.com"
}

while true; do clear; date; echo; commands; sleep 2; done
