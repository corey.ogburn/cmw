#!/bin/bash -ex

PRIMARY_PROJECTS="catalog-cache cmwnext cmwnext-config cmwnext-favorites cmwnext-ota cmwnext-parents cmwnext-presentation cmwnext-resumes cmwnext-rsdvr cmwnext-user"
OBSOLETE_PROJECTS="cmwnext-mediator" # Not migrated

# With proto-cache
DEPENDENCY_PROJECTS="cmwnext-api go-app go-cassandra go-claims go-cmwpostclient go-common go-config go-health go-http go-interaction-flag go-interaction-id go-kafka go-log go-metrics go-session-id go-session go-tracing go-zaplog proto-cache proto-cmwnext proto-config proto-favorites proto-mediator proto-ota proto-parents proto-presentation proto-progress proto-resumes proto-rsdvr proto-search proto-user go-local-dev"

ALL_PROJECTS="$PRIMARY_PROJECTS $DEPENDENCY_PROJECTS"

TARGET=~/go/src/gitlab.com/dish-cloud/sling/sling/hydra/
if [[ -e "$TARGET" ]]; then
    echo "Folder already exists, using it"
    cd "$TARGET"
else
    mkdir -p "$TARGET"
    cd "$TARGET"

    for project in $ALL_PROJECTS; do
        git clone git@gitlab.com:dish-cloud/sling/hydra/$project.git
    done
fi

find . -type f \( -name '*.go' -o -name '*.toml' \) -exec sed -i '' 's@p-bitbucket.imovetv.com/hydra@gitlab.com/dish-cloud/sling/sling/hydra@g' "{}" \;

# Append .git to dependency names (required to work around https://github.com/golang/dep/issues/1396)
for project in $ALL_PROJECTS; do
    find . -type f \( -name '*.go' -o -name '*.toml' \) -exec sed -i '' "s@gitlab.com/dish-cloud/sling/sling/hydra/$project\"@gitlab.com/dish-cloud/sling/sling/hydra/$project.git\"@g" "{}" \;
    find . -type f -name '*.go' -exec sed -i '' "s@gitlab.com/dish-cloud/sling/sling/hydra/$project/@gitlab.com/dish-cloud/sling/sling/hydra/$project.git/@g" "{}" \;
done

for project in $PRIMARY_PROJECTS; do
    cd $project
    dep ensure
    ./build.sh
    cd ..
done
