#!/bin/bash

if [ -z "$1" ]; then
    echo "Usage: $0 <bootstrap-server>"
    exit 1
fi
docker exec cmw-kafka kafka-topics --list --bootstrap-server "$1"

# local: localhost:9092
# dev: d-gp2-cmwkfk2-1.imovetv.com
