#!/bin/bash -ex

# q-gp2-cmwkfk3-1.imovetv.com:9092

docker-compose -f ~/go/src/gitlab.com/dish-cloud/sling/hydra/go-local-dev/docker-compose.yml \
    exec cmw-kafka-schema-registry kafka-avro-console-consumer \
    --bootstrap-server d-gp2-cmwkfk2-1.imovetv.com:9092 --topic bigdata_progresspoints --skip-message-on-error \
    --property schema.registry.url=http://q-gp2-cmwsr.imovetv.com
